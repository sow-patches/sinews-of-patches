package main

import "math/rand"

func qsort(a []float64, b []string, c [][]float64) ([]float64, []string, [][]float64) {
	if len(a) < 2 { return a, b, c }

	left, right := 0, len(a) - 1

	// Pick a pivot
	pivotIndex := rand.Int() % len(a)

	// Move the pivot to the right
	a[pivotIndex], a[right] = a[right], a[pivotIndex]
	b[pivotIndex], b[right] = b[right], b[pivotIndex]
	c[pivotIndex], c[right] = c[right], c[pivotIndex]

	// Pile elements smaller than the pivot on the left
	for i := range a {
		if a[i] < a[right] {
			a[i], a[left] = a[left], a[i]
			b[i], b[left] = b[left], b[i]
			c[i], c[left] = c[left], c[i]
			left++
		}
	}

	// Place the pivot after the last smaller element
	a[left], a[right] = a[right], a[left]
	b[left], b[right] = b[right], b[left]
	c[left], c[right] = c[right], c[left]

	// Go down the rabbit hole
	qsort(a[:left], b[:left], c[:left])
	qsort(a[left + 1:], b[left + 1:], c[left + 1:])


	return a, b, c
}

func preSort(a []float64, b []string) {
	for i := 0; i < len(a); i++ {
		if b[i] == "farmlands" {
			a[i] += 0.000095
		} else if b[i] == "floodplains" {
			a[i] += 0.000094
		} else if b[i] == "oasis" {
			a[i] += 0.000093
		} else if b[i] == "plains" {
			a[i] += 0.000092
		}else if b[i] == "drylands" {
			a[i] += 0.000091
		} else if b[i] == "steppe" {
			a[i] += 0.000090
		} else if b[i] == "desert" {
			a[i] += 0.000089
		} else if b[i] == "hills" {
			a[i] += 0.000088
		} else if b[i] == "mountains" {
			a[i] += 0.000087
		} else if b[i] == "desert_mountains" {
			a[i] += 0.000086
		} else if b[i] == "forest" {
			a[i] += 0.000085
		} else if b[i] == "taiga" {
			a[i] += 0.000084
		} else if b[i] == "wetlands" {
			a[i] += 0.000083
		}
	}
}
