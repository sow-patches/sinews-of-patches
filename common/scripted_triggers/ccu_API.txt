﻿###################################################################
# Purpose: Checks if two culture share a heritage group
# Scope: culture
# Arguments: target (culture)
# Sample Usage: culture:xxx = { has_same_heritage_group_as = { TARGET = culture:yyy } }

has_same_heritage_group_as = {
	var:heritage_group = $TARGET$.var:heritage_group
}

###################################################################
# Purpose: Checks if two culture share a heritage family
# Scope: culture
# Arguments: target (culture)
# Sample Usage: culture:xxx = { has_same_heritage_family_as = { TARGET = culture:yyy } }

has_same_heritage_family_as = {
	var:heritage_family = $TARGET$.var:heritage_family
}

###################################################################
# Purpose: Checks if two culture share a language group
# Scope: culture
# Arguments: target (culture)
# Sample Usage: culture:xxx = { has_same_language_group_as = { TARGET = culture:yyy } }

has_same_language_group_as = {
	var:language_group = $TARGET$.var:language_group
}

###################################################################
# Purpose: Checks if two culture share a language family
# Scope: culture
# Arguments: target (culture)
# Sample Usage: culture:xxx = { has_same_language_family_as = { TARGET = culture:yyy } }

has_same_language_family_as = {
	var:language_family = $TARGET$.var:language_family
}