﻿

new_faith_created_conversion_effect = {
	
	# Spouses convert
	scope:recipient = {
		every_spouse = {
			limit = {
				OR = {
					is_courtier_of = scope:actor
					is_courtier_of = scope:recipient
					is_vassal_of = scope:actor
					is_vassal_of = scope:recipient
				}
			}
			set_character_faith = scope:actor.faith
			hidden_effect = {
				add_character_flag = {
					flag = converted_by_forced_conversion_interaction
					years = 5
				}
			}
		}
	}
	# Family in recipient's court also convert
	if = {
		limit = {
			scope:recipient = {
				is_ruler = yes
				any_close_or_extended_family_member = {
					exists = court_owner
					court_owner = scope:recipient
					faith = scope:recipient.faith
					NOT = { faith = scope:actor.faith }
					is_ai = yes
				}	
			}	
		}
		scope:recipient = {
			every_close_or_extended_family_member = {
				custom = all_family_members_at_court
				limit = {
					exists = court_owner
					court_owner = scope:recipient
					faith = scope:recipient.faith
					NOT = { faith = scope:actor.faith }
					is_ai = yes
				}
				set_character_faith = scope:actor.faith
				hidden_effect = {
					add_character_flag = {
						flag = converted_by_forced_conversion_interaction
						years = 5
					}
				}
			}
		}
	}
	scope:recipient = {
		set_character_faith = scope:actor.faith
		hidden_effect = {
			add_character_flag = {
				flag = converted_by_forced_conversion_interaction
				years = 5
			}
		}
	}
	# Convert Capital (if of same faith as recipient's old faith)
	# don't convert shit	
}

adopt_religion_interaction_effect = {
	# Spouses convert
	if = {
		limit = {
			scope:actor = {
				any_spouse = {
					NOT = { faith = scope:recipient.faith }
					OR = {
						is_courtier_of = scope:actor
						is_courtier_of = scope:recipient
						is_vassal_of = scope:actor
						is_vassal_of = scope:recipient
					}
				}
			}
		}
		scope:actor = {
			every_spouse = {
				limit = {
					NOT = { faith = scope:recipient.faith }
					OR = {
						is_courtier_of = scope:actor
						is_courtier_of = scope:recipient
						is_vassal_of = scope:actor
						is_vassal_of = scope:recipient
					}
				}
				set_character_faith = scope:recipient.faith
			}
		}
	}
	# Family in actor's court also convert
	if = {
		limit = {
			scope:actor = {
				is_ruler = yes
				any_close_or_extended_family_member = {
					exists = court_owner
					court_owner = scope:actor
					faith = scope:actor.faith
					NOT = { faith = scope:recipient.faith }
					is_ai = yes
				}	
			}	
		}
		scope:actor = {
			every_close_or_extended_family_member = {
				custom = all_family_members_at_court_converison
				limit = {
					exists = court_owner
					court_owner = scope:actor
					faith = scope:actor.faith
					NOT = { faith = scope:recipient.faith }
					is_ai = yes
				}
				set_character_faith = scope:recipient.faith
			}
		}
	}
	scope:actor = {
		set_character_faith = scope:recipient.faith
	}
	# Convert Capital (if of same faith as actor's old faith)
	# nope	
}