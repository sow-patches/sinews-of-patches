﻿
############

# Raiding handled through vanilla events

on_county_occupied = {
	on_actions = { demd_on_county_occupied }
}

demd_on_county_occupied = {
	events = {
		demd_pop_war.1003
	}
}

############

on_faith_created = {
	on_actions = { demd_faith_created }
}

demd_faith_created = {
	effect = {
		faith = {
			faithEconomy = yes
		}
	}
}

############

on_culture_created = {
	on_actions = { demd_culture_created }
}

demd_culture_created = {
	effect = {
		# set advancement based on parent cultures
		set_variable = { name = culture_advancement value = 1 }
		every_parent_culture = {
			limit = { prev.var:culture_advancement = { compare_value < prev.var:culture_advancement } }
			prev = { set_variable = { name = culture_advancement value = prev.var:culture_advancement } }
		}
		set_variable = { name = vigor value = 100 }
		# set culture data
		cultureEconomy = yes
	}
}

############

on_title_gain = {
	on_actions = { 
		demd_title_gain 
		demd_title_gain_ai
		demd_ruler_pulse
	}
}

on_title_lost = {
	on_actions = { 
		demd_ruler_pulse
	}
}

demd_title_gain = {
	trigger = { 
		exists = scope:previous_holder
		scope:title = {
			tier = tier_county 
		}
	}
	effect = {
		remove_variable = tax_setting_changed
		remove_variable = manpower_setting_changed
		remove_variable = public_order_setting_changed
		remove_variable = sanitation_setting_changed
		remove_variable = irrigation_setting_changed
		remove_variable = infrastructure_setting_changed
	}
}

demd_title_gain_ai = {
	trigger = { 
		exists = scope:previous_holder
		scope:previous_holder = { is_ai = no } 
		scope:title = {
			tier = tier_county 
			holder = { is_ai = yes }
		}
	}
	effect = {
		scope:title = {
			# if player has fucked up edicts, unfuck them
			remove_variable = tax_setting_changed
			remove_variable = manpower_setting_changed
			remove_variable = public_order_setting_changed
			remove_variable = sanitation_setting_changed
			remove_variable = irrigation_setting_changed
			remove_variable = infrastructure_setting_changed
			
			# reset to factory
			demd_set_edict_setting = { TYPE = tax LEVEL = 2 }
			demd_set_edict_setting = { TYPE = manpower LEVEL = 2 }
			demd_set_edict_setting = { TYPE = public_order LEVEL = 2 }
			demd_set_edict_setting = { TYPE = sanitation LEVEL = 2 }
			demd_set_edict_setting = { TYPE = irrigation LEVEL = 2 }
			demd_set_edict_setting = { TYPE = infrastructure LEVEL = 2 }
		
			# let AI make changes twice
			
			remove_variable = tax_setting_changed
			remove_variable = manpower_setting_changed
			remove_variable = public_order_setting_changed
			remove_variable = sanitation_setting_changed
			remove_variable = irrigation_setting_changed
			remove_variable = infrastructure_setting_changed
			
			setEdicts = yes
			
			remove_variable = tax_setting_changed
			remove_variable = manpower_setting_changed
			remove_variable = public_order_setting_changed
			remove_variable = sanitation_setting_changed
			remove_variable = irrigation_setting_changed
			remove_variable = infrastructure_setting_changed
			
			setEdicts = yes
		}	
	}
}